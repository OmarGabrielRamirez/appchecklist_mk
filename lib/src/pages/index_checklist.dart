import 'package:appoperativamkm/src/pages/checklist_suc_page.dart';
import 'package:appoperativamkm/src/pages/create_checklist.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class IndexChecklist extends StatefulWidget {
  final String idCheckType;
  final String puntaje;
  final String nameChecklist;
  IndexChecklist(this.idCheckType, this.nameChecklist, this.puntaje);

  @override
  IndexChecklistState createState() => IndexChecklistState();
}

class IndexChecklistState extends State<IndexChecklist> {
  bool _loadingSuc = false, _notData = false;
  String _idCheck, _nameCheck, _puntaje, _token, _idTypeCheck;
  int _idUser;
  double _height;
  List<Sucursal> _scrs = List<Sucursal>();
  List<Sucursal> sucursales = List<Sucursal>();

  @override
  void initState() {
    super.initState();
    _idCheck = widget.idCheckType;
    _nameCheck = widget.nameChecklist;
    _puntaje = widget.puntaje;
    getDataSucCheck(_idCheck).then((value) {
      setState(() {
        sucursales.addAll(value);
        _scrs = sucursales;
        _loadingSuc = true;
      });
    });
  }

  Future<List<Sucursal>> getDataSucCheck(String _idCheck) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    _idUser = sharedPreferences.getInt("id_user");
    _token = sharedPreferences.getString("access_token");
    _idTypeCheck = _idCheck;

    Map data = {
      'idUser': _idUser.toString(),
      'access_token': _token,
      'idTypeCheck': _idTypeCheck
    };
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/sucursalesbycheck",
        body: data);
    var resBody = json.decode(response.body);
    if (response.body != "[]") {
      var suc = List<Sucursal>();
      for (var x in resBody) {
        Sucursal sucursal = Sucursal(x['id'], x['nombre'], x['totalCheck']);
        suc.add(sucursal);
      }
      return suc;
    } else {
      setState(() {
        _notData = true;
      });
    }
  }

  _searchBar() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 20),
            child: Text(
              "Checklist - " + _nameCheck,
              style: TextStyle(color: Colors.white, fontFamily: 'Lato'),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10, bottom: 10, left: 15, right: 15),
            child: TextFormField(
              onChanged: (text) {
                text = text.toLowerCase();
                setState(() {
                  _scrs = sucursales.where((sucursal) {
                    var nombreSucursal = sucursal.nombre.toLowerCase();
                    return nombreSucursal.contains(text);
                  }).toList();
                });
              },
              style: TextStyle(
                  fontFamily: 'Lato',
                  fontWeight: FontWeight.w300,
                  color: Colors.white,
                  fontSize: 16),
              keyboardType: TextInputType.text,
              cursorColor:
                  Color(0xFFB71C1C), //This will obscure text dynamically
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top: 30, bottom: 5),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 1.0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 1.0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 1.0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.white,
                  size: 18.0,
                ),
                labelStyle: TextStyle(
                    fontFamily: 'Lato',
                    fontWeight: FontWeight.w300,
                    fontSize: 13,
                    color: Colors.white),
                labelText: 'Buscar sucursal MK',
              ),
            ),
          ),
        ],
      ),
    );
  }

  _listSucursales(index, tamanio) {
    return Container(
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => ChecklistSucPage(
                  _idCheck, _nameCheck, _scrs[index].id, _scrs[index].nombre),
            ),
          );
        },
        child: Card(
          elevation: 20,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: ListTile(
            title: Text(
              _scrs[index].nombre,
              style: TextStyle(
                  fontFamily: 'Lato',
                  fontWeight: FontWeight.bold,
                  fontSize: 14),
            ),
            leading: Icon(Icons.store, color: Colors.red),
            trailing: Icon(
              Icons.navigate_next,
              color: Colors.black.withOpacity(0.8),
            ),
          ),
        ),
      ),
    );
  }

  _contentSuc(int _tamanio, bool _loadingSuc, bool _notData) {
    if (_notData == true) {
      return Expanded(
          child: Center(
        child: JumpingText(
          'Aun no se han agregado registros...',
          style: TextStyle(
              fontFamily: 'Lato',
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 12),
        ),
      ));
    } else if (_notData == false) {
      if (_tamanio != 0) {
        return Expanded(
            child: ListView.builder(
          padding: EdgeInsets.only(top: 10, bottom: 25, left: 15, right: 15),
          itemBuilder: (context, index) {
            return _listSucursales(index, _scrs.length);
          },
          itemCount: _scrs.length,
        ));
      } else if (_tamanio == 0 && _loadingSuc == false) {
        return Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SpinKitCircle(
                color: Colors.white,
                size: 40.0,
              ),
              JumpingText(
                'Cargando...',
                style: TextStyle(
                    fontFamily: 'Lato',
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ),
            ],
          ),
        );
      } else if (_tamanio == 0 && _loadingSuc == true) {
        return Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              JumpingText(
                'Sin coincidencias...',
                style: TextStyle(
                    fontFamily: 'Lato',
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ),
            ],
          ),
        );
      }
    }
  }

  _selectContainer() {
    return Container(
        child: Column(
      children: <Widget>[
        _searchBar(),
        _contentSuc(_scrs.length, _loadingSuc, _notData)
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    _height = queryData.size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        color: Colors.blueGrey[900],
        child: _selectContainer(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) =>
                  CreateChecklist(_idCheck, _nameCheck, _puntaje),
            ),
          );
        },
        backgroundColor: Colors.red,
        child: Icon(Icons.add, size: 30, color: Colors.white),
      ),
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.pop(context);
            }),
        backgroundColor: Colors.red,
        title: Text(
          "Visualizar resultados",
          style: TextStyle(
              fontFamily: 'Lato',
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color: Colors.white),
        ),
      ),
    );
  }
}

class Sucursal {
  final int id;
  final String nombre;
  final int checksTotal;
  const Sucursal(this.id, this.nombre, this.checksTotal);
}
