import 'package:appoperativamkm/src/pages/view_results_page.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:http/http.dart' as http;
import 'package:progress_indicators/progress_indicators.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:convert';

class ViewSubCheckPage extends StatefulWidget {
  String idTypeCheck, idSub;
  int idCheck, numberPage, totalPages, ptjFinally, ptjDes;

  ViewSubCheckPage(this.idTypeCheck, this.idCheck, this.idSub, this.numberPage,
      this.totalPages, this.ptjFinally, this.ptjDes);
  @override
  _ViewSubCheckPageState createState() => _ViewSubCheckPageState();
}

class _ViewSubCheckPageState extends State<ViewSubCheckPage> {
  bool img;
  int _numberPage,
      _totalPages,
      _totalPuntaje = 0,
      selectedIndex = 0,
      _ptjDes = 0,
      _idCheck;
  String _idTypeCheck,
      _idCheckActually,
      _comments = "Cargando...",
      _sect = "",
      _nombre = "Cargando...",
      nameImage = "";
  List<SubCheck> subchecks = [];
  List<Item> items = [];

  getDataSubs(String _idTypeCheck, int numberP, int totalPages) async {
    Map data = {
      'idTypeCheck': _idTypeCheck,
    };
    var response = await http
        .post("https://intranet.prigo.com.mx/api/subdatachecksids", body: data);
    var dataSub = json.decode(response.body);
    for (var x in dataSub) {
      SubCheck sub = SubCheck(x['idSub']);
      subchecks.add(sub);
    }
    setState(() {
      if (numberP == 0) {
        _sect = subchecks[0].idSub;
      } else if (numberP <= totalPages || numberP != totalPages) {
        _sect = subchecks[numberP].idSub;
      } else {
        _sect = "No data";
      }
    });
    getName(_sect);
    getDataItems(_sect, _idCheck);
    getDataItemsExtra(_sect, _idCheck);
  }

  getName(String _sect) async {
    Map data = {
      'idTypeCheck': _sect,
    };
    var response = await http
        .post("https://intranet.prigo.com.mx/api/subdatachecks", body: data);
    var dataSub = json.decode(response.body);
    setState(() {
      _nombre = dataSub[0]['nombre'];
    });

    return _nombre;
  }

  getDataItems(String idSub, int idCheck) async {
    Map dataPost = {'idCheck': idCheck.toString(), 'idSubCheck': idSub};
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/getdataitemschecks",
        body: dataPost);
    var data = json.decode(response.body);
    for (var i in data) {
      Item item = Item(i["idPregunta"], i["accion"], i["estado"], i["critico"]);
      setState(() {
        items.add(item);
      });
    }
    return items;
  }

  getDataItemsExtra(String idSub, int idCheck) async {
    Map dataPost = {'idCheck': idCheck.toString(), 'idSubCheck': idSub};
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/getdataextracheck",
        body: dataPost);
    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      setState(() {
        nameImage = data[0]['nombres_imagenes'];
        _comments = data[0]['comentarios'];
      });
      if (nameImage == "image_no_required") {
        setState(() {
          img = false;
        });
      } else {
        setState(() {
          img = true;
        });
      }
    }
  }

  Widget setButton(int numP, int limit) {
    Widget widget;
    if (numP < limit) {
      widget = IconButton(
          icon: Icon(Icons.arrow_forward),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ViewSubCheckPage(
                    _idTypeCheck,
                    _idCheck,
                    _sect,
                    _numberPage + 1,
                    _totalPages,
                    _totalPuntaje,
                    _ptjDes),
              ),
            );
          });
    } else if (numP == limit) {
      widget = IconButton(
          icon: Icon(Icons.arrow_forward),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) =>
                    ViewResultsCheckListPage(_idCheck, _idTypeCheck, _totalPuntaje),
              ),
            );
          });
    }
    return widget;
  }

  containerTotalImage(nameImage) {
    if (nameImage == "image_no_required") {
      return Container();
    } else {
      return Expanded(
        child: ListView(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Container(
                padding: EdgeInsets.only(top: 20),
                child: Text("Fotografía enviada por el responsable.",
                    style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'Lato',
                        fontWeight: FontWeight.bold,
                        color: Colors.black)),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 50, right: 50),
              width: MediaQuery.of(context).size.width - 150,
              height: MediaQuery.of(context).size.width - 20,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                child: containerImage(nameImage),
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.touch_app),
                  Text(
                    "Puede hacer zoom al dar doble click en la imagen o utilizando dos dedos.",
                    style: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 9,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    }
  }

  containerImage(nameImage) {
    if (nameImage == "") {
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SpinKitCircle(
              color: Colors.black,
              size: 40.0,
            ),
            JumpingText(
              'Cargando...',
              style: TextStyle(
                  fontFamily: 'Lato',
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 12),
            ),
          ],
        ),
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: PhotoView(
          imageProvider: CachedNetworkImageProvider(
              'https://intranet.prigo.com.mx/storage/check/' + nameImage),
        ),
      );
    }
  }

  Widget _detailsItems(_suct) {
    Widget w;
    if (_suct != '') {
      Widget icon;
      Widget iconT;
      w = ListView.builder(
          padding: EdgeInsets.only(bottom: 10, top: 10),
          itemCount: items.length,
          itemBuilder: (BuildContext context, int index) {
            if (items[index].critico == 1) {
              icon = Icon(
                Icons.assistant_photo,
                size: 17,
                color: Colors.red,
              );
            } else if (items[index].critico == 0) {
              icon = Icon(
                Icons.assistant_photo,
                size: 17,
                color: Colors.blueGrey[900],
              );
            }

            if (items[index].status == 1) {
              iconT = Icon(
                Icons.check,
                color: Colors.green,
                size: 17,
              );
            } else if (items[index].status == 0) {
              iconT = Icon(
                Icons.close,
                size: 17,
                color: Colors.red,
              );
            }
            return ListTile(
              leading: icon,
              title: Text(
                items[index].action,
                style: TextStyle(
                    fontFamily: 'Lato',
                    fontSize: 14.5,
                    fontWeight: FontWeight.w800,
                    color: Colors.black),
              ),
              trailing: iconT,
            );
          });
    } else {
      w = Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SpinKitCircle(
              color: Colors.black,
              size: 40.0,
            ),
            JumpingText(
              'Cargando...',
              style: TextStyle(
                  fontFamily: 'Lato',
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 12),
            ),
          ],
        ),
      );
    }
    return w;
  }

  selectContainer(index) {
    if (index == 0) {
      return Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        child: Card(
          elevation: 10,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: Column(children: <Widget>[
            Expanded(
              child: _detailsItems(_sect),
            )
          ]),
        ),
      );
    } else if (index == 1) {
      return Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        child: Card(
          elevation: 10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Column(
            children: <Widget>[
              containerTotalImage(nameImage),
              Container(
                        height:   MediaQuery.of(context).size.width- 200,
                  padding: EdgeInsets.only(top:20,bottom:40, left:20, right:20),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 5,
                    child: ListTile(
                      contentPadding: EdgeInsets.all(10),
                      leading: Icon(Icons.textsms),
                      title: Text(
                        "Comentarios: ",
                        style: TextStyle(
                            fontFamily: 'Lato', fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        _comments,
                        style: TextStyle(
                            fontFamily: 'Lato', fontWeight: FontWeight.bold),
                      ),
                    ),
                  )),
            ],
          ),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _numberPage = widget.numberPage;
      _totalPages = widget.totalPages;
      _idTypeCheck = widget.idTypeCheck;
      _totalPuntaje = widget.ptjFinally;
      _idCheckActually = widget.idSub;
      _ptjDes = widget.ptjDes;
      _idCheck = widget.idCheck;
    });
    getDataSubs(_idTypeCheck, _numberPage, _totalPages);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: FFNavigationBar(
        theme: FFNavigationBarTheme(
            barHeight: 60,
            barBackgroundColor: Colors.blueGrey[900],
            selectedItemBackgroundColor: Colors.blueGrey[900],
            selectedItemIconColor: Colors.white,
            selectedItemLabelColor: Colors.black,
            unselectedItemTextStyle:
                TextStyle(fontFamily: 'Lato', fontSize: 14),
            selectedItemTextStyle:
                TextStyle(letterSpacing: 0, fontFamily: 'Lato', fontSize: 14),
            unselectedItemLabelColor: Colors.white38,
            unselectedItemIconColor: Colors.white38),
        selectedIndex: selectedIndex,
        onSelectTab: (index) {
          setState(() {
            selectedIndex = index;
          });
        },
        items: [
          FFNavigationBarItem(
            iconData: Icons.list,
            label: 'Checklist',
            selectedLabelColor: Colors.white,
          ),
          FFNavigationBarItem(
            iconData: Icons.camera_enhance,
            label: 'Extras',
            selectedLabelColor: Colors.white,
          ),
        ],
      ),
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.blueGrey[900],
      appBar: AppBar(
        actions: <Widget>[setButton(_numberPage, _totalPages)],
        centerTitle: true,
        title: Text(
          "#" + _idCheck.toString() + " Detalles - " + _nombre,
          style: TextStyle(
              fontFamily: 'Lato',
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color: Colors.white),
        ),
      ),
      body: selectContainer(selectedIndex),
    );
  }
}

class SubCheck {
  String idSub;
  SubCheck(this.idSub);
}

class SubCheckList {
  final String nombre;
  SubCheckList(this.nombre);
}

class Item {
  final int idItem;
  final String action;
  final int status;
  final int critico;

  Item(
    this.idItem,
    this.action,
    this.status,
    this.critico,
  );
}
