import 'package:appoperativamkm/src/pages/sub_checklist_page.dart';
import 'package:appoperativamkm/src/pages/subchecklist_page.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:status_alert/status_alert.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:convert';

class CreateChecklist extends StatefulWidget {
  final String idTypeChecklist;
  final String puntaje;
  final String name;

  CreateChecklist(this.idTypeChecklist, this.name, this.puntaje);

  @override
  CreateChecklistState createState() => CreateChecklistState();
}

class CreateChecklistState extends State<CreateChecklist> {
  String _selection,
      _selectionR,
      formattedDate,
      _idTypeCheck,
      _name,
      _puntaje,
      _token;
  int idUser, totalPages, _numidCheck;
  List dataSucursales = List();
  List dataResponsable = List();
  List<SubCheck> subchecks = [];
  String primerSect;
  getResponsables(String seleccion) async {
    Map data = {'id': seleccion};

    var response = await http
        .post("https://intranet.prigo.com.mx/api/getresponsables", body: data);
    var resBody = json.decode(response.body);
    setState(() {
      dataResponsable = resBody;
    });
  }

  getData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      idUser = sharedPreferences.getInt("id_user");
      _token = sharedPreferences.getString("access_token");
    });
    Map data = {'idUser': idUser.toString(), 'access_token': _token};
    var response = await http
        .post("https://intranet.prigo.com.mx/api/checksucursales", body: data);
    setState(() {
      var resBody = json.decode(response.body);
      dataSucursales = resBody;
    });
  }

  getLastId() async {
    var response =
        await http.get("https://intranet.prigo.com.mx/api/getidfinally");
    if (response.statusCode == 200) {
      if (response.body == "") {
        setState(() {
          _numidCheck = 1;
        });
      } else {
        var resBody = json.decode(response.body);
        setState(() {
          _numidCheck = resBody + 1;
        });
      }
    }
  }

  getDataSubs(String _idTypeCheck) async {
    Map data = {
      'idTypeCheck': _idTypeCheck,
    };
    var response = await http
        .post("https://intranet.prigo.com.mx/api/subdatachecksids", body: data);
    var dataSub = json.decode(response.body);
    for (var x in dataSub) {
      SubCheck sub = SubCheck(x['idSub']);
      subchecks.add(sub);
    }
    setState(() {
      totalPages = subchecks.length;
      primerSect = subchecks[0].idSub;
    });
  }

  showAlert(String title, BuildContext context) {
    StatusAlert.show(
      context,
      blurPower: 10,
      titleOptions: StatusAlertTextConfiguration(
        style: TextStyle(
          fontFamily: 'Lato',
          fontWeight: FontWeight.bold,
          fontSize: 14,
        ),
      ),
      duration: Duration(seconds: 2),
      title: title,
      configuration: IconConfiguration(icon: AntDesign.warning),
    );
  }

  saveData(BuildContext context) async {
    var selectedDate = new DateTime.now();
    var formatter = new DateFormat('yyyy-dd-MM kk:mm');
    String date = formatter.format(selectedDate);
    if (date != null && _selection != null && _selectionR != null) {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setString('idTypeCheck', _idTypeCheck);
      sharedPreferences.setInt('numidCheck', _numidCheck);
      sharedPreferences.setString("dateUser", formattedDate);
      sharedPreferences.setString("dateAuto", date);
      sharedPreferences.setString("idSuc", _selection);
      sharedPreferences.setString("gerente", _selectionR);

      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => SubChecklistPage(_idTypeCheck,
              _numidCheck.toString(), primerSect, 0, totalPages - 1, 0),
        ),
      );
    } else if (_selection == null) {
      showAlert("Selecciona una sucursal.", context);
    } else if (_selectionR == null) {
      showAlert("Selecciona un gerente o responsable.", context);
    }
  }

  @override
  void initState() {
    super.initState();
    this.getData();
    this.getLastId();
    setState(() {
      DateTime now = DateTime.now();
      formattedDate = DateFormat('yyyy-dd-MM kk:mm').format(now);
      _idTypeCheck = widget.idTypeChecklist;
      getDataSubs(_idTypeCheck);
      _name = widget.name;
      _puntaje = widget.puntaje;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.red,
        title: Text(
          "Crear nuevo",
          style: TextStyle(
              fontFamily: 'Lato', fontWeight: FontWeight.w500, fontSize: 14),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
        width: MediaQuery.of(context).size.width - 40,
        child: Card(
          elevation: 10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: ListView(
            children: <Widget>[
              Container(
                child: Column(children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 40),
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        child: Text("Nuevo checklist",
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.bold,
                                color: Colors.black)),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        child: Text(_name,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.bold,
                                color: Colors.black45)),
                      ),
                    ),
                  ),
                ]),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, left: 65, right: 65),
                child: Column(children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 15, bottom: 15),
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        child: Text("Fecha / Hora de visita ",
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.bold,
                                color: Colors.black)),
                      ),
                    ),
                  ),
                  Container(
                    height: 48,
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.black,
                          width: 0.5,
                        ),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.date_range,
                          size: 18,
                          color: Colors.red,
                        ),
                        // SizedBox(
                        //   width: 4,
                        // ),
                        FlatButton(
                          onPressed: () {
                            DatePicker.showDateTimePicker(
                              context,
                              showTitleActions: true,
                              minTime: DateTime.now(),
                              maxTime: DateTime(2025, 12, 12, 24, 00),
                              onChanged: (date) {},
                              onConfirm: (date) {
                                setState(() {
                                  formattedDate = DateFormat('yyyy-dd-MM kk:mm')
                                      .format(date);
                                });
                              },
                              currentTime: DateTime.now(),
                              locale: LocaleType.es,
                            );
                          },
                          child: Text(
                            formattedDate,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Lato',
                              fontSize: 13,
                            ),
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              IconButton(
                                icon: Icon(Icons.keyboard_arrow_down),
                                color: Colors.red,
                                onPressed: () {
                                  DatePicker.showDateTimePicker(
                                    context,
                                    showTitleActions: true,
                                    minTime: DateTime.now(),
                                    maxTime: DateTime(2025, 12, 12, 24, 00),
                                    onChanged: (date) {},
                                    onConfirm: (date) {
                                      setState(() {
                                        formattedDate =
                                            DateFormat('yyyy-dd-MM kk:mm')
                                                .format(date);
                                      });
                                    },
                                    currentTime: DateTime.now(),
                                    locale: LocaleType.es,
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ]),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, left: 32, right: 32),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            child: Text("Sucursal ",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontFamily: 'Lato',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black)),
                          ),
                        ),
                      ),
                      Container(
                        height: 48,
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Colors.black,
                              width: 0.5,
                            ),
                          ),
                        ),
                        child: DropdownButton(
                          hint: Container(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(width: 10),
                              Icon(Icons.store, size: 18, color: Colors.red),
                              SizedBox(width: 10),
                              Text(
                                'Selecciona una sucursal',
                                style: TextStyle(
                                    fontFamily: 'Lato',
                                    fontSize: 13,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(width: 10),
                            ],
                          )),
                          underline: Container(color: Colors.white),
                          icon: Container(
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.keyboard_arrow_down,
                                  color: Colors.red,
                                ),
                                SizedBox(width: 10)
                              ],
                            ),
                          ),
                          items: dataSucursales.map((item) {
                            return DropdownMenuItem(
                                value: item['id'].toString(),
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(width: 10),
                                    Icon(Icons.store,
                                        size: 18.0, color: Colors.red),
                                    SizedBox(width: 30),
                                    Center(
                                      child: Text(
                                        item['nombre'],
                                        style: TextStyle(
                                            fontFamily: 'Lato',
                                            fontSize: 13.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    )
                                  ],
                                ));
                          }).toList(),
                          onChanged: (newVal) {
                            setState(() {
                              _selection = newVal;
                              _selectionR = null;
                              getResponsables(_selection);
                            });
                          },
                          value: _selection,
                        ),
                      )
                    ]),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, left: 32, right: 32),
                child: Column(children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 15, bottom: 15),
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        child: Text(" Gerente / Responsable ",
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.bold,
                                color: Colors.black)),
                      ),
                    ),
                  ),
                  Container(
                    height: 48,
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.black,
                          width: 0.5,
                        ),
                      ),
                    ),
                    child: DropdownButton(
                      hint: Container(
                          child: Row(
                        children: <Widget>[
                          SizedBox(width: 10),
                          Icon(Icons.perm_identity,
                              size: 18, color: Colors.red),
                          SizedBox(width: 10),
                          Text(
                            'Selecciona un responsable',
                            style: TextStyle(
                                fontFamily: 'Lato',
                                fontSize: 13,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          ),
                          SizedBox(width: 5),
                        ],
                      )),
                      underline: Container(color: Colors.white),
                      icon: Container(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.keyboard_arrow_down,
                              color: Colors.red,
                            ),
                            SizedBox(width: 10)
                          ],
                        ),
                      ),
                      items: dataResponsable.map((item) {
                        return DropdownMenuItem(
                            value: item['nombre'].toString(),
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.perm_identity,
                                    size: 18.0, color: Colors.red),
                                SizedBox(width: 10),
                                Text(
                                  item['nombre'],
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontSize: 13.0,
                                      fontWeight: FontWeight.bold),
                                ),
                    
                              ],
                            ));
                      }).toList(),
                      onChanged: (newVal) {
                        setState(() {
                          _selectionR = newVal;
                        });
                      },
                      value: _selectionR,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 40,
                    padding: EdgeInsets.only(top: 40),
                    child: GestureDetector(
                      onTap: () {
                        saveData(context);
                      },
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                            color: Colors.red[600],
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Center(
                          child: Text(
                            'Siguiente',
                            style: TextStyle(
                                fontFamily: 'Lato',
                                fontSize: 15.0,
                                fontWeight: FontWeight.w300,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ]),
              )
            ],
          ),
        ),
      ),
      backgroundColor: Colors.blueGrey[900],
    );
  }
}

class SubCheck {
  String idSub;
  SubCheck(this.idSub);
}
