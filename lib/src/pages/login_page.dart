import 'dart:convert';
import 'dart:ui';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  bool _obscureText;
  bool _isLoading = false;
  AnimationController _controller;
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
    _obscureText = true;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  notRotate() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    double _scale = 1 - _controller.value;

    notRotate();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        // color: Colors.blueGrey[800],
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new ExactAssetImage('assets/images/sidebar-1.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: new BackdropFilter(
          filter: new ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
          child: Container(
            padding: EdgeInsets.only(top: 70),
            alignment: Alignment.center,
            color: Colors.blueGrey[900].withOpacity(1),
            child: _isLoading
                ? Center(
                    child: SpinKitFadingCircle(
                    color: Colors.white,
                  ))
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 250,
                        margin: EdgeInsets.only(
                            top: 10, bottom: 10, left: 50, right: 50),
                        child: Center(
                          child: Image.asset("assets/images/mk_logo.png"),
                        ),
                      ),
                      Container(
                        alignment: Alignment.bottomCenter,
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: TextField(
                          style: TextStyle(
                              fontFamily: 'Lato',
                              fontWeight: FontWeight.w300,
                              color: Colors.white,
                              fontSize: 16),
                          controller: _emailController,
                          cursorColor: Color(0xFFB71C1C),
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.email,
                              size: 17.0,
                              color: Colors.white,
                            ),
                            contentPadding:
                                EdgeInsets.only(top: 20, bottom: 10),
                            hintStyle: TextStyle(
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w300,
                                fontSize: 16,
                                color: Colors.white70),
                            labelStyle: TextStyle(
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w300,
                                fontSize: 16,
                                color: Colors.white),
                            labelText: "Email",
                            hintText: "ejemplo@prigo.com.mx",
                            enabledBorder: const UnderlineInputBorder(
                              borderSide: const BorderSide(
                                width: 0.7,
                                color: Colors.white,
                              ),
                            ),
                            border: const OutlineInputBorder(
                              borderSide: const BorderSide(width: 1.0),
                            ),
                            focusedBorder: const UnderlineInputBorder(
                              borderSide: const BorderSide(
                                width: 1,
                                color: Color(0XFFEF5350),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 30, right: 30, top: 13),
                        child: TextFormField(
                          style: TextStyle(
                              fontFamily: 'Lato',
                              fontWeight: FontWeight.w300,
                              color: Colors.white,
                              fontSize: 16),
                          keyboardType: TextInputType.text,
                          controller: _passwordController,
                          cursorColor: Color(0xFFB71C1C),
                          obscureText:
                              _obscureText, //This will obscure text dynamically
                          decoration: InputDecoration(
                            helperStyle: TextStyle(
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w300,
                                color: Colors.white,
                                fontSize: 9),
                            helperText: '¿Problemas? sit@prigo.com.mx',
                            contentPadding: EdgeInsets.only(top: 20, bottom: 5),
                            enabledBorder: const UnderlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.white, width: 0.7),
                            ),
                            border: const OutlineInputBorder(
                              borderSide: const BorderSide(width: 1.0),
                            ),
                            focusedBorder: const UnderlineInputBorder(
                              borderSide: const BorderSide(
                                width: 1,
                                color: Color(0XFFEF5350),
                              ),
                            ),
                            prefixIcon: Icon(
                              Icons.vpn_key,
                              color: Colors.white,
                              size: 18.0,
                            ),
                            labelStyle: TextStyle(
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w300,
                                fontSize: 16,
                                color: Colors.white),
                            labelText: 'Contraseña',
                            suffixIcon: IconButton(
                              icon: Icon(
                                _obscureText
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                setState(() {
                                  _obscureText = !_obscureText;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                      containerButton(_scale),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  Widget containerButton(double _scale) {
    return Container(
      height: 70,
      padding: EdgeInsets.only(top: 15, left: 30, right: 30),
      child: GestureDetector(
        onTapDown: _onTapDown,
        onTapUp: _onTapUp,
        child: Transform.scale(
          scale: _scale,
          child: _animatedButtonUI,
        ),
      ),
    );
  }

  Widget get _animatedButtonUI => Container(
        height: 60,
        decoration: BoxDecoration(
          color: Colors.red[600],
          borderRadius: BorderRadius.circular(15.0),
          boxShadow: [
            BoxShadow(
              color: Color(0x80000000),
              blurRadius: 10.0,
              offset: Offset(0.0, 1.0),
            ),
          ],
        ),
        child: Center(
          child: Text(
            'Ingresar',
            style: TextStyle(
                fontFamily: 'Lato',
                fontSize: 20.0,
                fontWeight: FontWeight.w300,
                color: Colors.white),
          ),
        ),
      );

  void _onTapDown(TapDownDetails details) {
    setState(() {
      _isLoading = true;
    });
    signIn(_emailController.text, _passwordController.text, context);
    _controller.forward();
  }

  void _onTapUp(TapUpDetails details) {
    _controller.reverse();
  }

  signIn(String email, String password, BuildContext context) async {
    Map data = {'email': email, 'password': password};

    var jsonData;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    var response = await http
        .post("https://intranet.prigo.com.mx/api/auth/login", body: data);

    if (response.statusCode == 200) {
      jsonData = json.decode(response.body);
      if (jsonData != null) {
        setState(() {
          _isLoading = false;
        });
      }

      Map dataInfo = {
        'id_user': jsonData['id'].toString(),
        'access_token': jsonData['access_token']
      };
      var info = await http.post(
          "https://intranet.prigo.com.mx/api/getinfouser",
          body: dataInfo);
      var jsonDataInfo = json.decode(info.body);
      sharedPreferences.setString("name_user", jsonDataInfo['name']);
      sharedPreferences
          .setString("email_user", jsonDataInfo['email'])
          .toString();
      sharedPreferences.setInt("id_user", jsonData['id']);
      sharedPreferences.setString("access_token", jsonData['access_token']);
      Navigator.pushReplacementNamed(context, '/');
    } else {
      setState(() {
        _isLoading = false;
      });
      _showAlert(context);
    }
  }

  void _showAlert(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Icon(Icons.error_outline, color: Colors.red, size: 70.0),
            content: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Text("Ingresa los datos correctamente para poder iniciar sesión.",
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 15.0,
                      fontFamily: 'Lato')),
            ]),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  "OK",
                  style: TextStyle(color: Colors.red[600]),
                ),
              )
            ],
          );
        });
  }
}
