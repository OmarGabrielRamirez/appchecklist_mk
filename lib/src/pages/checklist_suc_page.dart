import 'package:appoperativamkm/src/pages/show_checklist_page.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ChecklistSucPage extends StatefulWidget {
  final String idCheckType;
  final String nameChecklist;
  final int idSuc;
  final String nameSuc;

  ChecklistSucPage(
      this.idCheckType, this.nameChecklist, this.idSuc, this.nameSuc);

  @override
  ChecklistSucPageState createState() => ChecklistSucPageState();
}

class ChecklistSucPageState extends State<ChecklistSucPage> {
  bool _loadingSuc = true;
  String _idCheckType;
  String _nameChecklist;
  String _actualDate;
  int _idSuc, selectedIndex = 0;
  String _nameSuc;

  List<CheckList> _checkstotales = List<CheckList>();
  List<CheckList> _checksttlsrch = List<CheckList>();
  List<CheckList> _checksday = List<CheckList>();
  @override
  void initState() {
    super.initState();
    DateTime date = DateTime.now();
    setState(() {
      _actualDate = DateFormat('yyyy-MM-dd').format(date);
      _idCheckType = widget.idCheckType;
      _nameChecklist = widget.nameChecklist;
      _idSuc = widget.idSuc;
      _nameSuc = widget.nameSuc;
    });
    getDataChecksTotales().then((value) {
      setState(() {
        _checkstotales.addAll(value);
        _checksttlsrch = _checkstotales;
        _checksday = _checkstotales.where((check) {
          var dateCheck = check.fecha.toLowerCase().substring(0, 19 - 9);
          return dateCheck.contains(_actualDate.toLowerCase());
        }).toList();
      });
    });
  }

  Future<List<CheckList>> getDataChecksTotales() async {
    Map data = {'idCheckList': _idCheckType, 'idSuc': _idSuc.toString()};
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/getdatacheksbysuc",
        body: data);
    var resBody = json.decode(response.body);
    var checks = List<CheckList>();
    if (resBody != "error/not_data") {
      if (resBody.length != 0) {
        setState(() {
          _loadingSuc = true;
        });
      }
      for (var x in resBody) {
        CheckList check = CheckList(x['idCheckList'], x['responsable'],
            x['fechaGenerada'], x['puntajeFinal'], x['puntajeDeseado']);
        checks.add(check);
      }
      return checks;
    } else {
      return checks;
    }
  }

  _checksTotal(int tamanio) {
    if (tamanio == 0) {
      return Container(
        color: Colors.blueGrey[900],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.warning,
              color: Colors.white,
              size: 45,
            ),
            Text(
              "Sin registros totales.",
              style: TextStyle(color: Colors.white, fontFamily: 'Lato'),
            )
          ],
        ),
      );
    } else {
      return Container(
        color: Colors.blueGrey[900],
        child: Container(
          child: ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 20, bottom: 20, left: 10, right: 10),
            itemBuilder: (context, index) {
              String fecha = _checkstotales[index].fecha.toString();
              return Container(
                child: GestureDetector(
                  onTap: () {

                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ShowCheckListPage(
                            _checkstotales[index].idCheckList, _nameChecklist, _idCheckType),
                      ),
                    );
                  },
                  child: Container(
                    child: Padding(
                      padding: EdgeInsets.only(left: 5, right: 5, bottom: 5),
                      child: Card(
                        color: Colors.white,
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Column(children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 20, top: 5, right: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Icon(MdiIcons.textBoxCheck,
                                            size: 17, color: Colors.red),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                           "#"+ _checkstotales[index].idCheckList.toString()+" "+ _nameChecklist,
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              shadows: [
                                                Shadow(
                                                  blurRadius: 1.5,
                                                  color: Colors.white,
                                                  offset: Offset(1.0, 0.5),
                                                ),
                                              ],
                                              fontFamily: 'Lato'),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 20, top: 5, right: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Icon(Icons.date_range,
                                            size: 17, color: Colors.red),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          fecha.substring(0, 18 - 2),
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              shadows: [
                                                Shadow(
                                                  blurRadius: 1.5,
                                                  color: Colors.white,
                                                  offset: Offset(1.0, 0.5),
                                                ),
                                              ],
                                              fontFamily: 'Lato'),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 20, top: 5, right: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Icon(Icons.assessment,
                                            size: 17, color: Colors.red),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Text(
                                          _checkstotales[index]
                                                  .puntajeFinal
                                                  .toString() +
                                              " / " +
                                              _checkstotales[index]
                                                  .puntajeDeseado,
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              shadows: [
                                                Shadow(
                                                  blurRadius: 1.5,
                                                  color: Colors.white,
                                                  offset: Offset(1.0, 0.5),
                                                ),
                                              ],
                                              fontFamily: 'Lato'),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 20, top: 5, right: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Icon(Icons.perm_identity,
                                            size: 17, color: Colors.red),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 1),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          _checkstotales[index].responsable,
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              shadows: [
                                                Shadow(
                                                  blurRadius: 1.5,
                                                  color: Colors.white,
                                                  offset: Offset(1.0, 0.5),
                                                ),
                                              ],
                                              fontFamily: 'Lato'),
                                        ),
                                      ),
                                    ],
                                  ),
                                ]),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
            itemCount: _checkstotales.length,
          ),
        ),
      );
    }
  }

  _checksDay(int tamanio) {
    if (tamanio == 0) {
      return Container(
        color: Colors.blueGrey[900],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.warning,
              color: Colors.white,
              size: 45,
            ),
            Text(
              "Sin registros del día.",
              style: TextStyle(color: Colors.white, fontFamily: 'Lato'),
            ),
            Text(
              _actualDate,
              style: TextStyle(color: Colors.white, fontFamily: 'Lato'),
            ),
          ],
        ),
      );
    } else {
      return Container(
        color: Colors.blueGrey[900],
        child: Container(
          child: ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 20, bottom: 20, left: 10, right: 10),
            itemBuilder: (context, index) {
              String fecha = _checkstotales[index].fecha.toString();
              return Container(
                child: GestureDetector(
                  onTap: () {

                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ShowCheckListPage(
                            _checkstotales[index].idCheckList, _nameChecklist, _idCheckType),
                      ),
                    );
                  },
                  child: Container(
                    child: Padding(
                      padding: EdgeInsets.only(left: 5, right: 5, bottom: 5),
                      child: Card(
                        color: Colors.white,
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Column(children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 20, top: 5, right: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Icon(MdiIcons.textBoxCheck,
                                            size: 17, color: Colors.red),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                        "#"+ _checkstotales[index].idCheckList.toString()+" "+ _nameChecklist,
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              shadows: [
                                                Shadow(
                                                  blurRadius: 1.5,
                                                  color: Colors.white,
                                                  offset: Offset(1.0, 0.5),
                                                ),
                                              ],
                                              fontFamily: 'Lato'),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 20, top: 5, right: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Icon(Icons.date_range,
                                            size: 17, color: Colors.red),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          fecha.substring(0, 18 - 2),
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              shadows: [
                                                Shadow(
                                                  blurRadius: 1.5,
                                                  color: Colors.white,
                                                  offset: Offset(1.0, 0.5),
                                                ),
                                              ],
                                              fontFamily: 'Lato'),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 20, top: 5, right: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Icon(Icons.assessment,
                                            size: 17, color: Colors.red),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Text(
                                          _checksday[index]
                                                  .puntajeFinal
                                                  .toString() +
                                              " / " +
                                              _checksday[index].puntajeDeseado,
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              shadows: [
                                                Shadow(
                                                  blurRadius: 1.5,
                                                  color: Colors.white,
                                                  offset: Offset(1.0, 0.5),
                                                ),
                                              ],
                                              fontFamily: 'Lato'),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 20, top: 5, right: 5),
                                        alignment: Alignment.centerLeft,
                                        child: Icon(Icons.perm_identity,
                                            size: 17, color: Colors.red),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 1),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          _checksday[index].responsable,
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              shadows: [
                                                Shadow(
                                                  blurRadius: 1.5,
                                                  color: Colors.white,
                                                  offset: Offset(1.0, 0.5),
                                                ),
                                              ],
                                              fontFamily: 'Lato'),
                                        ),
                                      ),
                                    ],
                                  ),
                                ]),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
            itemCount: _checksday.length,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: choices.length,
      child: Scaffold(
        body: TabBarView(children: [
          _checksDay(_checksday.length),
          _checksTotal(_checkstotales.length),
        ]),
        appBar: AppBar(
          bottom: TabBar(
            tabs: choices.map<Widget>((ItemTab choice) {
              return Tab(
                child: Container(
                    child: Column(
                  children: <Widget>[
                    Container(
                      child: Text(
                        choice.title,
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'Lato',
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 3),
                      child: Icon(
                        choice.icon,
                        size: 20,
                      ),
                    )
                  ],
                )),
              );
            }).toList(),
          ),
          title: Text(
            "Visualizar resultados - " + _nameSuc,
            style: TextStyle(
                fontFamily: 'Lato',
                fontWeight: FontWeight.w500,
                fontSize: 14,
                color: Colors.white),
          ),
        ),
      ),
    );
  }
}

class CheckList {
  int idCheckList;
  String responsable;
  String fecha;
  int puntajeFinal;
  String puntajeDeseado;

  CheckList(this.idCheckList, this.responsable, this.fecha, this.puntajeFinal,
      this.puntajeDeseado);
}

class ItemTab {
  final String title;
  final IconData icon;
  const ItemTab({this.title, this.icon});
}

const List<ItemTab> choices = <ItemTab>[
  ItemTab(title: 'Del día', icon: MdiIcons.timer),
  ItemTab(title: 'Totales', icon: MdiIcons.allInclusive),
];
