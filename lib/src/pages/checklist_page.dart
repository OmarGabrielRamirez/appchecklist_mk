import 'dart:convert';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:appoperativamkm/src/bloc.navigation_menu/bloc_navigation.dart';
import 'package:appoperativamkm/src/pages/index_checklist.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ChecklistPage extends StatefulWidget with NavigationStates {
  @override
  ChecklistPageState createState() => ChecklistPageState();
}

class ChecklistPageState extends State<ChecklistPage> with NavigationStates {
  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  notRotate() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  checkLoginStatus() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("access_token") == null) {
      Navigator.pushNamed(context, 'login');
    }
  }

  Future<List<CheckList>> getDataCheckList() async {
    var response =
        await http.get("https://intranet.prigo.com.mx/api/datachecks");
    var dataSub = json.decode(response.body);
    List<CheckList> checks = [];
    for (var x in dataSub) {
      CheckList check =
          CheckList(x['idCheckList'], x['nombre'], x['puntaje_total']);
      checks.add(check);
    }
    return checks;
  }

  @override
  Widget build(BuildContext context) {
    notRotate();
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.blueGrey[900],
        body: Container(
          child: FutureBuilder(
              future: getDataCheckList(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SpinKitCircle(
                          color: Colors.white,
                          size: 40.0,
                        ),
                        JumpingText(
                          'Cargando...',
                          style: TextStyle(
                              fontFamily: 'Lato',
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Container(
                    margin: EdgeInsets.only(
                        top: 45, left: 20, right: 20, bottom: 45),
                    child: AnimationLimiter(
                      child: ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return AnimationConfiguration.staggeredList(
                              position: index,
                              duration: const Duration(milliseconds: 675),
                              child: SlideAnimation(
                                verticalOffset: 70.0,
                                child: FlipAnimation(
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: 5, right: 5, bottom: 5),
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                IndexChecklist(
                                                    snapshot.data[index].id,
                                                    snapshot.data[index].name,
                                                    snapshot
                                                        .data[index].puntaje),
                                          ),
                                        );
                                      },
                                      child: Card(
                                        elevation: 20,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                        child: Container(
                                          child: Column(children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(top: 20),
                                              child: Text(
                                                snapshot.data[index].name,
                                                style: TextStyle(
                                                    fontFamily: 'Lato',
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 15),
                                              ),
                                            ),
                                            Container(
                                                padding: EdgeInsets.only(
                                                    top: 10, bottom: 20),
                                                child: Icon(MdiIcons.checkAll,
                                                    size: 30)),
                                          ]),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
                  );
                }
              }),
        ),
      ),
    );
  }
}

class CheckList {
  final String id;
  final String name;
  final String puntaje;

  CheckList(this.id, this.name, this.puntaje);
}
